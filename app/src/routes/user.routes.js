const express = require('express')
const passport = require('../../utilities/passports/user.passport')
const controllers = require('../../config/controllers/index')
const route = express.Router()

// routes auth
route.post('/user/auth', controllers.authController.login)

// middleware for JWT auth
const isAuthenticated = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user) => {
        if (err || !user) {
            console.error('Authentication error:', err)
            return res.status(401).json({ error: 'Unauthorized' })
        }
        next()
    })(req, res, next)
}

// routes don't need authenticate
route.post('/', controllers.userController.create)

// routes need authenticate
route.use(isAuthenticated)
route.get('/user', controllers.userController.findAll)
route.get('/user/:id', controllers.userController.findOne)
route.put('/user/:id', controllers.userController.update)
route.delete('/user/:id', controllers.userController.remove)

route.get('/city', controllers.cityController.findAll)
route.get('/city/:id', controllers.cityController.findOne)

route.get('/thread', controllers.threadController.findAll)
route.get('/thread/comment', controllers.threadController.findOne)
route.post('/thread/create', controllers.threadController.create)
// route.put('/thread/:id', controllers.threadController.update) FIXME:
route.delete('/thread/remove', controllers.threadController.remove)

route.get('/thread/like/load', controllers.likedController.findAll)
route.get('/thread/like', controllers.likedController.findOne)
route.post('/thread/like', controllers.likedController.likeThread)

route.get('/thread/comment/load', controllers.commentController.findAll)
route.post('/thread/comment/new', controllers.commentController.commentThread)

module.exports = (app) => {
    app.use('/api', route)
}