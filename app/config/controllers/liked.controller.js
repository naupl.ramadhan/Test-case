const jwt = require('jsonwebtoken')
const errorHandler = require('../handlers/error.handler')
const moment = require('moment-timezone')
const models = require('../../src/models/index')
const User = models.User
const Thread = models.Thread
const Like = models.Like

// get all likes
exports.findAll = async(req, res) => {
    try {
        const {threadID} = req.query
        const LikeID = await Like.find({threadID})

        if (!LikeID || LikeID.length === 0) {
            return res
                .status(404)
                .json({message: 'be the first to like!'})
        }

        const likeCount = LikeID.length
        const getID = LikeID.map(like => like.userID)
        const users = await User.find({
            _id: {
                $in: getID
            }
        })

        const userIdMap = {}
        users.forEach(user => {
            userIdMap[user._id] = user.name
        })

        const userInfo = getID.map(id => ({id, name: userIdMap[id]}))

        const formatted = LikeID.map(likes => {
            const jakartaTime = moment(likes.createdAt)
                .tz('Asia/Jakarta')
                .format()
            return {
                id: likes._id,
                user: userInfo.find(user => user.id === likes.userID),
                like: likes.like,
                date: jakartaTime
            }
        })

        res
            .status(200)
            .json({threadID: threadID, likeCount: likeCount, likes: formatted})
    } catch (err) {
        errorHandler(err, res)
    }
}

// get one like
exports.findOne = async(req, res) => {
    try {
        const {threadID} = req.query
        const LikeID = await Like.find({threadID})
        const like = await Like.findById(LikeID)

        if (!like) {
            return res
                .status(404)
                .json({message: 'Like not found'})
        }

        const user = await User.findById(like.userID)

        if (!user) {
            return res
                .status(404)
                .json({message: 'User not found'})
        }

        const userInfo = {
            id: user._id,
            name: user.name
        }

        const jakartaTime = moment(like.createdAt)
            .tz('Asia/Jakarta')
            .format()

        const formatted = {
            id: like._id,
            user: userInfo,
            like: like.like,
            date: jakartaTime
        }

        res
            .status(200)
            .json(formatted)
    } catch (err) {
        errorHandler(err, res)
    }
}

// like thread
exports.likeThread = async(req, res) => {
    try {
        const {threadID} = req.query

        // Get token from headers
        const token = req
            .headers
            .authorization
            .split(' ')[1]
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET)

        // Decode ID from token
        const userID = decodedToken.sub // FIXME: ini nanti ganti sub jadi id
        if (!userID) {
            return res
                .status(401)
                .send('Authorization failed')
        }

        // thread exists?
        const threadExists = await Thread.exists({_id: threadID})
        if (!threadExists) {
            return res
                .status(404)
                .json({message: 'Thread not found'})
        }

        // does user already liked the selected thread?
        const existingLike = await Like.findOne({userID: userID, threadID: threadID})
        if (existingLike) {
            await Like.findByIdAndDelete(existingLike._id)
            return res
                .status(201)
                .json({message: 'disliked'})
        }

        await Like.create({userID: userID, threadID: threadID})

        return res
            .status(201)
            .json({message: 'liked'})
    } catch (err) {
        errorHandler(err, res)
    }
}