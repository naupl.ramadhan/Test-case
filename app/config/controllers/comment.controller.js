const jwt = require('jsonwebtoken')
const errorHandler = require('../handlers/error.handler')
const moment = require('moment-timezone')
const models = require('../../src/models/index')
const User = models.User
const Comment = models.Comment

exports.findAll = async(req, res) => {
    try {
        const {threadID} = req.query
        const commentID = await Comment.find({threadID})

        if (!commentID || commentID.length === 0) {
            return res
                .status(404)
                .json({message: 'wanna be the first to comment?'})
        }

        const getID = commentID.map(comment => comment.userID)
        const users = await User.find({
            _id: {
                $in: getID
            }
        })

        const userIdMap = {}
        users.forEach(user => {
            userIdMap[user._id] = user.name
        })

        const userInfo = getID.map(id => ({id, name: userIdMap[id]}))

        const formatted = commentID.map(comment => {
            const jakartaTime = moment(comment.createdAt)
                .tz('Asia/Jakarta')
                .format()
            return {
                id: comment._id,
                user: userInfo.find(user => user.id === comment.userID),
                comment: comment.comment,
                date: jakartaTime
            }
        })

        res.status(200).json({
            threadID: threadID,
            comments: formatted
        })
    } catch (err) {
        errorHandler(err, res)
    }
}

exports.commentThread = async(req, res) => {
    try {
        const {threadID} = req.query

        // get token from headers
        const token = req
            .headers
            .authorization
            .split(' ')[1]
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET)

        // decode ID from token
        const userID = decodedToken.sub // FIXME: ini nanti ganti sub jadi id
        if (!userID) {
            return res
                .status(401)
                .send('Authorization failed')
        }

        await Comment.create({userID: userID, threadID: threadID, comment: req.body.comment})
        res
            .status(201)
            .send('comment sent')
    } catch (err) {
        errorHandler(err, res)
    }
}