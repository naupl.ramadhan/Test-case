const authController = require('../../config/controllers/auth.controller')
const userController = require('../../config/controllers/user.controller')
const cityController = require('../../config/controllers/city.controller')
const threadController = require('../../config/controllers/thread.controller')
const likedController = require('../../config/controllers/liked.controller')
const commentController = require('../../config/controllers/comment.controller')

module.exports = {
    authController,
    userController,
    cityController,
    threadController,
    likedController,
    commentController
}