const moment = require('moment-timezone')
const models = require('../../../src/models/index')
const User = models.User
const Like = models.Like
const Comment = models.Comment

async function findOneThreadData(id) {
    try {
        const thread = await models.Thread.findById(id)
        
        if (!thread) {
            throw new Error('Thread not found')
        }

        const userInfo = await User.findOne({ _id: thread.userID }).select('id name')
        const likeCount = await Like.countDocuments({ threadID: thread._id })
        const commentCount = await Comment.countDocuments({ threadID: thread._id })
        const newDate = moment.tz(thread.createdAt, 'UTC').tz('Asia/Jakarta').format()

        // return data
        return {
            _id: thread._id,
            title: thread.title,
            text: thread.text,
            user: userInfo,
            likes: likeCount,
            comments: commentCount,
            date: newDate   
        }
    } catch (err) {
        throw err
    }
}

module.exports = findOneThreadData