const moment = require('moment-timezone')
const models = require('../../../src/models/index')
const User = models.User
const Like = models.Like
const Comment = models.Comment

async function findAllThreadData() {
    try {
        const threads = await models.Thread.find({})

        const threadData = await Promise.all(threads.map(async (thread) => {
            if (thread.isDeleted) {
                return null
            }
            
            const userInfo = await User.findOne({ _id: thread.userID }).select('id name')
            const likeCount = await Like.countDocuments({ threadID: thread._id })
            const commentCount = await Comment.countDocuments({ threadID: thread._id })
            const newDate = moment.tz(thread.createdAt, 'UTC').tz('Asia/Jakarta').format()

            return {
                _id: thread._id,
                title: thread.title,
                text: thread.text,
                user: userInfo,
                likes: likeCount,
                comments: commentCount,
                date: newDate
            }
        }))

        // Filter thread yang dihapus
        const filteredThreadData = threadData.filter(thread => thread !== null)

        return filteredThreadData
    } catch (err) {
        throw err
    }
}

module.exports = findAllThreadData