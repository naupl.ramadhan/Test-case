const mongoose = require('mongoose')
const url = "mongodb://localhost:27017/test_case"

// connection for city seeders
const connectDB = async () => {
  try {
    await mongoose.connect(url, {})
  } catch (err) {
    console.error(`Error connecting to the database: ${err.message}`)
    throw err
  }
}

// disconnecting seeders
const disconnectDB = async () => {
  try {
    await mongoose.disconnect()
  } catch (err) {
    console.error(`Error disconnecting from the database: ${err.message}`)
    throw err
  }
}

module.exports = {
  url,
  connectDB,
  disconnectDB
}